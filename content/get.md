---
title: Distributions offering Plasma Bigscreen
menu:
  main:
    weight: 4
    name: Install
sassFiles:
  - scss/get.scss
---

As of right now, Plasma Bigscreen isn't available for public use yet. This is due to not being developed for so long. The project has been revived, but it might take a while until it becomes stable and is available for public use. You can check the status of Plasma Bigscreen on the [Matrix channel](https://matrix.to/#/#plasma-bigscreen:kde.org) or the [KDE Invent repository](https://invent.kde.org/plasma/plasma-bigscreen).

<!-- ## postmarketOS

![](/img/pmOS.svg)

PostmarketOS (pmOS) is a pre-configured Alpine Linux that can be installed on smartphones, SBC's and other mobile devices. View the [device list](https://wiki.postmarketos.org/wiki/Devices) to see the progress for supporting your device.

For devices that do not have prebuilt images, you will need to flash it manually using the `pmbootstrap` utility.
Follow instructions [here](https://wiki.postmarketos.org/wiki/Installation_guide).
Be sure to also check the device's wiki page for more information on what is working.

[Learn more](https://postmarketos.org)

##### Download:

* [Community Devices](https://postmarketos.org/download/)

## Manjaro ARM

![](/img/manjaro.png)

Manjaro ARM is an Arch based distribution with support for a wide range of single board computers, ARM laptops and phones.

Manjaro ARM currently offers Bigscreen images for Odroid N2 and N2+, Radxa Zero 2, RockPro64, Raspberry Pi 4, Khadas Vim 1 and Khadas Vim 3.

##### Download:

* [Development images](https://github.com/manjaro-arm/bigscreen/releases)

### Installation

Simply download the Bigscreen image for the device you have, extract it so you have the raw .img file, then flash it to your drive using `dd` or a graphical tool. Then plug in the drive to your device and power it on.

## Debian

![](/img/debian.png)

Debian has Plasma Bigscreen available as a [package ](https://packages.debian.org/stable/kde/plasma-bigscreen) in their repositories. -->