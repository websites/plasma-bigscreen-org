---
title: Screenshots
layout: page
screenshots:
  - url: /img/pages/screenshots/Bigscreen1.png
    name: "Plasma Bigscreen homescreen"
  - url: /img/pages/screenshots/Bigscreen2.png
    name: "Settings -> Appearance and personalization"
  - url: /img/pages/screenshots/Bigscreen3.png
    name: "Settings -> Display"
  - url: /img/pages/screenshots/Bigscreen4.png
    name: "Favorites"
menu:
  main:
    parent: project
    weight: 2
---

{{< screenshots name="screenshots" >}}
