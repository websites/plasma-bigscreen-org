---
title: Introduction
weight: 1
---

## Can I use it?

Yes, but not as a daily driver.
There are currently multiple distributions shipping Plasma Bigscreen in their unstable/nightly repositories.

The interface is using KWin over Wayland and is now mostly stable, albeit a little rough around the edges in some areas.
A subset of the normal KDE Plasma features are available.
This makes it possible to use and develop for Plasma Bigscreen on your desktop/laptop.

## What can it do?

There are quite a few TV-optimized apps that are now being bundled with the various distributions, allowing a wide range of basic functions.
These are mostly built with Kirigami, KDE’s interface framework allowing convergent UIs that work very well in a TV remote-only environment.

## Where can I find…

The code for the shell can be found on [invent.kde.org](https://invent.kde.org/plasma/plasma-bigscreen).

You can also ask your questions in the [Plasma Bigscreen community groups and channels](/join).
