#
# SPDX-FileCopyrightText: 2021, 2022, 2023, 2024 Vincenzo Reale <smart2128vr@gmail.com>
# Paolo Zamponi <feus73@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: 1.0\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-12-06 01:51+0000\n"
"PO-Revision-Date: 2024-12-06 22:18+0100\n"
"Last-Translator: Vincenzo Reale <smart2128vr@gmail.com>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 24.08.3\n"

#: config.yaml:0
msgid "A privacy-respecting, open source and secure TV ecosystem"
msgstr ""
"Ecosistema per televisori rispettoso della privacy, open source e sicuro"

#: config.yaml:0
msgid "Project"
msgstr "Progetto"

#: config.yaml:0
msgid "Documentation"
msgstr "Documentazione"

#: config.yaml:0
msgid "Community"
msgstr "Comunità"

#: content/_index.md:0 i18n/en.yaml:0
msgid "Plasma Bigscreen"
msgstr "Plasma Bigscreen"

#: content/faq.md:0
msgid "Frequently Asked Questions"
msgstr "Domande frequenti"

#: content/faq.md:0
msgid "FAQ"
msgstr "FAQ"

#: content/faq.md:11
msgid "What happened to Mycroft AI?"
msgstr "Che fine ha fatto Mycroft AI?"

#: content/faq.md:13
msgid ""
"Mycroft AI was discontinued and bought by a company named OVOS. Therefore, "
"we won't use them for voice-related stuff anymore. Refer to a FAQ they made "
"about the situation [here](https://community.openconversational.ai/t/faq-"
"ovos-neon-and-the-future-of-the-mycroft-voice-assistant/13496)."
msgstr ""
"Mycroft AI è stato interrotto e acquistato da una società chiamata OVOS. "
"Pertanto, non lo utilizzeremo più per cose relative alla voce. Fai "
"riferimento a una FAQ che hanno creato sulla situazione [qui](https://"
"community.openconversational.ai/t/faq-ovos-neon-and-the-future-of-the-"
"mycroft-voice-assistant/13496)."

#: content/faq.md:15
msgid "Help! My TV remote does not work, how do I fix it?"
msgstr ""
"Aiuto! Il telecomando del mio televisore non funziona, come posso risolvere?"

#: content/faq.md:17
msgid ""
"Many TV manufacturers require HDMI-CEC mode to be enabled manually, one "
"needs to refer to the TV user guide to figure out how to configure the HDMI-"
"CEC option on their television set. The HDMI-CEC option can be found under "
"various names depending on the TV manufacturers, some for example: (TCL TV: "
"T-Link, Panasonic TV: Viera Link, Samsung TV: Anynet+, Sony TV: Bravia Sync)."
msgstr ""
"Molti produttori di televisori richiedono l'abilitazione manuale della "
"modalità HDMI-CEC, è necessario fare riferimento alla guida dell'utente del "
"televisore per capire come configurare l'opzione HDMI-CEC. L'opzione HDMI-"
"CEC può essere trovata con vari nomi a seconda dei produttori di televisori, "
"alcuni ad esempio: (TV TCL: T-Link, TV Panasonic: Viera Link, TV Samsung: "
"Anynet+, TV Sony: Bravia Sync)."

#: content/faq.md:20
msgid ""
"Also make sure your Plasma Bigscreen device is actually supported by libcec, "
"[check their supported hardware](https://github.com/Pulse-Eight/"
"libcec#supported-hardware)."
msgstr ""
"Assicurati anche che il tuo dispositivo Plasma Bigscreen sia effettivamente "
"supportato da libcec, [controlla l'hardware supportato] (https://github.com/"
"Pulse-Eight/libcec#supported-hardware)."

#: content/faq.md:22
msgid ""
"Some buttons on my TV remote are not working, I can’t exit an application or "
"use the back button properly. How do I fix it?"
msgstr ""
"Alcuni pulsanti sul telecomando del mio televisore non funzionano, non "
"riesco a uscire da un'applicazione o a utilizzare correttamente il pulsante "
"Indietro. Come risolvo?"

#: content/faq.md:24
msgid ""
"HDMI-CEC on the beta image is in a testing phase. We have only been able to "
"test the functionality on a few selected range of TV sets and have mapped TV "
"remotes on the basis of those working devices. You can map and test your TV "
"remote following a few simple steps of debugging and editing files listed "
"below."
msgstr ""
"HDMI-CEC sull'immagine beta è in fase di test. Siamo stati in grado di "
"provare la funzionalità solo su alcune serie selezionate di televisori e "
"abbiamo mappato i telecomandi TV sulla base di quei dispositivi funzionanti. "
"Puoi mappare e testare il telecomando del tuo televisore seguendo alcuni "
"semplici passaggi di debug e modifica dei file elencati di seguito."

#: content/faq.md:28
msgid "_Test if the KEY is working with HDMI-CEC and extract it’s KEY CODE:_"
msgstr "_Verifica se la KEY funziona con HDMI-CEC ed estrai il suo KEY CODE:_"

#: content/faq.md:35
msgid ""
"Once the script is running, press the button on your TV remote to extract "
"its KEY CODE, if no KEY CODE is found the KEY might not be part of HDMI-CEC "
"controls enabled on your TV set, refer to the TV User Guide to know which "
"keys are enabled under your TV manufacturers HDMI-CEC implementation."
msgstr ""
"Una volta che lo script è in esecuzione, premere il pulsante sul telecomando "
"del televisore per estrarre il suo KEY CODE, se non viene trovato alcun KEY "
"CODE, la KEY potrebbe non far parte dei controlli HDMI-CEC abilitati sul "
"televisore, fare riferimento alla guida dell'utente del televisore per "
"sapere quali tasti sono abilitati nell'implementazione HDMI-CEC del "
"produttore."

#: content/faq.md:37
msgid "_Adding the found KEY CODE and mapping it in the CEC daemon:_"
msgstr "_Aggiunta del KEY CODE trovato e associazione nel demone CEC:_"

#: content/faq.md:44
msgid "Locate KEYMAP = {} in the daemon script"
msgstr "Individua KEYMAP = {} nello script del demone"

#: content/faq.md:45
msgid ""
"Add your KEY CODE in the following format to the list: “9: u.KEY\\_HOMEPAGE”"
msgstr ""
"Aggiungi il tuo KEY CODE all'elenco nel seguente formato: “9: u.KEY"
"\\_HOMEPAGE”"

#: content/faq.md:46
msgid ""
"In the above example “u.KEY\\_HOMEPAGE” is mapped to the home button that is "
"used to exit an application"
msgstr ""
"Nell'esempio seguente “u.KEY\\_HOMEPAGE” è mappato sul pulsante home "
"utilizzato per uscire da un'applicazione"

#: content/faq.md:47
msgid "“9” being the Key Code"
msgstr "“9” è il Key Code"

#: content/faq.md:48
msgid "“u.KEY\\_HOMEPAGE” being the action the key should perform"
msgstr "“u.KEY\\_HOMEPAGE” è l'azione che il tasto dovrebbe eseguire"

#: content/faq.md:50
msgid ""
"I have a generic USB remote but it’s missing the home key, how do I exit "
"applications?"
msgstr ""
"Ho un telecomando USB generico, ma manca il tasto home, come esco dalle "
"applicazioni?"

#: content/faq.md:52
msgid ""
"Not all generic USB remotes are built alike, therefore we recommend using a "
"tested product like the “Wechip G20 Air Mouse With Microphone Remote”. If in-"
"case you are unable to get your hands on one, you can still map an existing "
"key on the remote."
msgstr ""
"Non tutti i telecomandi USB generici sono costruiti allo stesso modo, quindi "
"consigliamo di utilizzare un prodotto testato come «Wechip G20 Air Mouse con "
"microfono». Nel caso in cui non riesci a trovarne uno, puoi comunque mappare "
"un tasto esistente sul telecomando."

#: content/faq.md:55
msgid "_Mapping the window close button to a button on a USB remote:_"
msgstr ""
"_Mappatura del pulsante di chiusura della finestra su un pulsante su un "
"telecomando USB:_"

#: content/faq.md:62
msgid "Find the entry “Window Close” located under `[Kwin]`"
msgstr "Trova la voce “Window Close” posizionata sotto `[Kwin]`"

#: content/faq.md:63
msgid "Assign your button to the “Window Close” entry"
msgstr "Assegna il tuo pulsante alla voce “Windows Close”"

#: content/faq.md:64
msgid ""
"Example: `Window Close=Alt+F4\\t’YourButtonHere’,Alt+F4\\t’YourButtonHere’,"
"Close Window`"
msgstr ""
"Esempio: `Window Close=Alt+F4\\t’IlTuoPulsanteQui’,Alt"
"+F4\\t’IlTuoPulsanteQui’,Close Window`"

#: content/faq.md:66
msgid "How do I exit applications using a external keyboard?"
msgstr "Come esco dalle applicazioni utilizzando una tastiera esterna?"

#: content/faq.md:68
msgid ""
"“Alt+F4” is the general shortcut assigned to closing applications using a "
"external keyboard. Custom keys can be assigned to the following file for "
"various actions:"
msgstr ""
"«Alt+F4» è la scorciatoia generale assegnata alla chiusura delle "
"applicazioni utilizzando una tastiera esterna. Le chiavi personalizzate "
"possono essere assegnate al seguente file per varie azioni:"

#: content/faq.md:76
msgid "How to contribute & upload your custom keymap for CEC?"
msgstr ""
"Come contribuire e caricare la tua mappa dei tasti personalizzata per CEC?"

#: content/faq.md:78
msgid ""
"Currently the development repository of the CEC daemon can be found at "
"https://invent.kde.org/adityam/easycec with instructions on how to add a "
"device with custom keymap."
msgstr ""
"Attualmente il deposito di sviluppo del demone CEC è disponibile su https://"
"invent.kde.org/adityam/easycec con le istruzioni su come aggiungere un "
"dispositivo con una mappa dei tasti personalizzata."

#: content/faq.md:80
msgid "Can Android apps work on Plasma Bigscreen?"
msgstr "Le applicazioni di Android funzionano in Plasma Bigscreen?"

#: content/faq.md:82
msgid ""
"There are projects out there like [Waydroid](https://waydro.id/) which is "
"Android running inside a Linux container, and use the Linux kernel to "
"execute applications to achieve near-native performance. This could be "
"leveraged in the future to have Android apps running on top of a GNU/Linux "
"system with the Plasma Bigscreen platform but it's a complicated task, and "
"as of *today* (October 29, 2021) some distributions already support Waydroid "
"and you can run Plasma Bigscreen on top of those distributions."
msgstr ""
"Ci sono progetti come [Waydroid](https://waydro.id/), che è Android in "
"esecuzione all'interno di un contenitore Linux e utilizza il kernel Linux "
"per eseguire applicazioni per ottenere prestazioni quasi native. Questo "
"potrebbe essere sfruttato in futuro per avere applicazioni Android in "
"esecuzione su un sistema GNU/Linux con la piattaforma Plasma Bigscreen, ma è "
"complicato. Ad *oggi* (29 ottobre 2021), alcune distribuzioni supportano già "
"Waydroid: puoi eseguire Plasma Bigscreen in queste distribuzioni."

#: content/faq.md:85
msgid "Can I run Plasma Bigscreen on my TV or setup-box?"
msgstr "Posso eseguire Plasma Bigscreen sul mio televisore o set-top box?"

#: content/faq.md:87
msgid "In theory, yes. In practice, it depends."
msgstr "In teoria, sì. In pratica, dipende."

#: content/faq.md:90
msgid ""
"Hardware support is not dictated by Plasma Bigscreen, instead it depends on "
"the devices supported by the distributions shipping it. See [the install "
"page](/get/) for more details of distributions shipping Plasma Bigscreen."
msgstr ""
"Il supporto hardware non è dettato da Plasma Bigscreen, ma dipende dai "
"dispositivi supportati dalle distribuzioni che lo rilasciano. Vedi [la "
"pagina di installazione](/get/) per maggiori dettagli sulle distribuzioni "
"che rilasciano Plasma Bigscreen."

#: content/faq.md:93
msgid "What's the state of the project?"
msgstr "Qual è lo stato del progetto?"

#: content/faq.md:95
msgid ""
"Plasma Bigscreen is currently under heavy development and is not intended to "
"be used as a daily driver."
msgstr ""
"Plasma Bigscreen è attualmente in una fase di sviluppo intenso, e non è "
"concepito per essere utilizzato come dispositivo principale."

#: content/get.md:0 i18n/en.yaml:0
msgid "Install"
msgstr "Installa"

#: content/get.md:0
msgid "Distributions offering Plasma Bigscreen"
msgstr "Distribuzioni che offrono Plasma Bigscreen"

#: content/get.md:11
msgid ""
"As of right now, Plasma Bigscreen isn't available for public use yet. This "
"is due to not being developed for so long. The project has been revived, but "
"it might take a while until it becomes stable and is available for public "
"use. You can check the status of Plasma Bigscreen on the [Matrix channel]"
"(https://matrix.to/#/#plasma-bigscreen:kde.org) or the [KDE Invent "
"repository](https://invent.kde.org/plasma/plasma-bigscreen)."
msgstr ""
"Al momento, Plasma Bigscreen non è ancora disponibile per l'uso pubblico. "
"Questo perché non è stato sviluppato per così tanto tempo. Il progetto è "
"stato ripreso, ma potrebbe volerci un po' prima che diventi stabile e "
"disponibile per l'uso pubblico. Puoi controllare lo stato di Plasma "
"Bigscreen sul [canale Matrix](https://matrix.to/#/\n"
"#plasma-bigscreen:kde.org) o sul [deposito di KDE Invent](https://invent.kde."
"org/plasma/plasma-bigscreen)."

#: content/get.md:13
msgid ""
"<!-- ## postmarketOS\n"
"\n"
"![](/img/pmOS.svg)\n"
"\n"
"PostmarketOS (pmOS) is a pre-configured Alpine Linux that can be installed "
"on smartphones, SBC's and other mobile devices. View the [device list]"
"(https://wiki.postmarketos.org/wiki/Devices) to see the progress for "
"supporting your device.\n"
"\n"
"For devices that do not have prebuilt images, you will need to flash it "
"manually using the `pmbootstrap` utility.\n"
"Follow instructions [here](https://wiki.postmarketos.org/wiki/"
"Installation_guide).\n"
"Be sure to also check the device's wiki page for more information on what is "
"working.\n"
"\n"
"[Learn more](https://postmarketos.org)\n"
"\n"
"##### Download:\n"
"\n"
"* [Community Devices](https://postmarketos.org/download/)\n"
"\n"
"## Manjaro ARM\n"
"\n"
"![](/img/manjaro.png)\n"
"\n"
"Manjaro ARM is an Arch based distribution with support for a wide range of "
"single board computers, ARM laptops and phones.\n"
"\n"
"Manjaro ARM currently offers Bigscreen images for Odroid N2 and N2+, Radxa "
"Zero 2, RockPro64, Raspberry Pi 4, Khadas Vim 1 and Khadas Vim 3.\n"
"\n"
"##### Download:\n"
"\n"
"* [Development images](https://github.com/manjaro-arm/bigscreen/releases)\n"
"\n"
"### Installation\n"
"\n"
"Simply download the Bigscreen image for the device you have, extract it so "
"you have the raw .img file, then flash it to your drive using `dd` or a "
"graphical tool. Then plug in the drive to your device and power it on.\n"
"\n"
"## Debian\n"
"\n"
"![](/img/debian.png)\n"
"\n"
"Debian has Plasma Bigscreen available as a [package ](https://packages."
"debian.org/stable/kde/plasma-bigscreen) in their repositories. -->"
msgstr ""
"<!-- ## postmarketOS\n"
"\n"
"![](/img/pmOS.svg)\n"
"\n"
"PostmarketOS (pmOS) è un Alpine Linux preconfigurato che può essere "
"installato su smartphone, SBC e altri dispositivi mobili. Visualizza "
"l'[elenco dispositivi](https://wiki.postmarketos.org/wiki/Devices) per "
"vedere i progressi nel supporto del tuo dispositivo.\n"
"\n"
"Per i dispositivi che non hanno immagini predefinite, dovrai scriverlo su un "
"memoria flash manualmente usando l'utilità `pmbootstrap`.\n"
"Segui le istruzioni [qui](https://wiki.postmarketos.org/wiki/"
"Installation_guide).\n"
"Assicurati di controllare anche la pagina wiki del dispositivo per maggiori "
"informazioni su cosa funziona.\n"
"\n"
"[Scopri di più](https://postmarketos.org)\n"
"\n"
"##### Scaricamento:\n"
"\n"
"* [Dispositivi della comunità](https://postmarketos.org/download/)\n"
"\n"
"## Manjaro ARM\n"
"\n"
"![](/img/manjaro.png)\n"
"\n"
"Manjaro ARM è una distribuzione basata su Arch con supporto per un'ampia "
"gamma di computer a scheda singola, laptop ARM e telefoni.\n"
"\n"
"Manjaro ARM offre attualmente immagini Bigscreen per Odroid N2 e N2+, Radxa "
"Zero 2, RockPro64, Raspberry Pi 4, Khadas Vim 1 e Khadas Vim 3.\n"
"\n"
"##### Scaricamento:\n"
"\n"
"* [Immagini di sviluppo](https://github.com/manjaro-arm/bigscreen/releases)\n"
"\n"
"### Installazione\n"
"\n"
"Scarica semplicemente l'immagine Bigscreen per il dispositivo che hai, "
"estraila in modo da avere il file .img grezzo, quindi scrivila sulla tua "
"memoria flash usando `dd` o uno strumento grafico. Quindi collega l'unità al "
"tuo dispositivo e accendilo.\n"
"\n"
"## Debian\n"
"\n"
"![](/img/debian.png)\n"
"\n"
"Debian ha Plasma Bigscreen disponibile come [pacchetto ](https://packages."
"debian.org/stable/kde/plasma-bigscreen) nei suoi depositi. -->"

#: content/join.md:0
msgid "Join Plasma Bigscreen"
msgstr "Unisciti a Plasma Bigscreen"

#: content/join.md:12
msgid ""
"If you'd like to contribute to the amazing free software for TV's and setup-"
"boxes, [join us - we always have a task for you](/contributing/)!"
msgstr ""
"Se desideri contribuire allo straordinario software libero per televisori e "
"set-top box, [unisciti a noi - abbiamo sempre qualcosa da farti fare](/"
"contributing/)!"

#: content/join.md:14
msgid "Plasma Bigscreen specific channels:"
msgstr "Canali specifici di Plasma Bigscreen:"

#: content/join.md:16
msgid ""
"[![](/img/matrix.svg)Matrix (most active)](https://matrix.to/#/#plasma-"
"bigscreen:kde.org)"
msgstr ""
"[![](/img/matrix.svg)Matrix (il più attivo)](https://matrix.to/#/#plasma-"
"bigscreen:kde.org)"

#: content/join.md:18
msgid "[![](/img/telegram.svg)Telegram](https://t.me/plasma_bigscreen)"
msgstr "[![](/img/telegram.svg)Telegram](https://t.me/plasma_bigscreen)"

#: content/join.md:21
msgid "Plasma Biscreen related project channels:"
msgstr "Canali di progetti relativi a Plasma Bigscreen:"

#: content/join.md:23
msgid ""
"[![](/img/irc.png)#plasma on Freenode (IRC)](https://kiwiirc.com/nextclient/"
"chat.freenode.net/#plasma)"
msgstr ""
"[![](/img/irc.png)#plasma su Freenode (IRC)](https://kiwiirc.com/nextclient/"
"chat.freenode.net/#plasma)"

#: content/join.md:25
msgid ""
"[![](/img/mail.svg)Plasma development mailing list](https://mail.kde.org/"
"mailman/listinfo/plasma-devel)"
msgstr ""
"[![](/img/mail.svg)lista di distribuzione sullo sviluppo di Plasma](https://"
"mail.kde.org/mailman/listinfo/plasma-devel)"

#: content/screenshots.md:0
msgid "Screenshots"
msgstr "Schermate"

#: content/screenshots.md:0
msgid "Plasma Bigscreen homescreen"
msgstr "Schermata iniziale di Plasma Bigscreen"

#: content/screenshots.md:0
msgid "Settings -> Appearance and personalization"
msgstr "Impostazioni -> Aspetto e personalizzazione"

#: content/screenshots.md:0
msgid "Settings -> Display"
msgstr "Impostazioni -> Schermo"

#: content/screenshots.md:0
msgid "Favorites"
msgstr "Preferiti"

#: content/vision.md:0
msgid "Our Vision"
msgstr "La nostra visione"

#: content/vision.md:0
msgid "Vision"
msgstr "Visione"

#: content/vision.md:12
msgid ""
"Plasma Bigscreen aims to become a complete and open software system for TV's "
"and setup-boxes.<br /> It is designed to give privacy-aware users back "
"control over their information, communication and media."
msgstr ""
"Plasma Bigscreen aspira a diventare un sistema software completo e aperto "
"per televisori e set-top box.<br />È progettato per ridare agli utenti "
"attenti alla riservatezza il controllo sulle loro informazioni e "
"comunicazioni."

#: content/vision.md:15
msgid ""
"Plasma Bigscreen takes a pragmatic approach and is inclusive to 3rd party "
"software, allowing the user to choose which applications and services to "
"use, while providing a seamless experience across multiple devices.<br /> "
"Plasma Bigscreen implements open standards and is developed in a transparent "
"process that is open for anyone to participate in."
msgstr ""
"Plasma Bigscreen adotta un approccio pragmatico ed è comprensivo di software "
"di terze parti, permettendo all'utente di scegliere quali applicazioni e "
"servizi utilizzare, fornendo un'esperienza unica su più dispositivi.<br /> "
"Plasma Bigscreen implementa standard aperti, ed è sviluppato in un processo "
"trasparente che è aperto per chiunque voglia parteciparvi."

#: i18n/en.yaml:0
msgid "Plasma on your TV"
msgstr "Plasma sul tuo televisore"

#: i18n/en.yaml:0
msgid "This project uses various open-source components like"
msgstr "Questo progetto utilizza vari componenti open source come"

#: i18n/en.yaml:0
msgid "Mycroft AI"
msgstr "Mycroft AI"

#: i18n/en.yaml:0
msgid "LibCEC"
msgstr "LibCEC"

#: i18n/en.yaml:0
msgid "Open up"
msgstr "Apri"

#: i18n/en.yaml:0
msgid ""
"Plasma Bigscreen is an open-source user interface for TV's. Running on top "
"of a Linux distribution, Plasma Bigscreen turns your TV or setup-box into a "
"fully hackable device. A big launcher giving you easy access to any "
"installed apps and skills. Controllable via voice or TV remote."
msgstr ""
"Plasma Bigscreen è un'interfaccia utente open source per televisori o set-"
"top box. Essendo eseguito sopra una distribuzione Linux, Plasma Bigscreen "
"trasforma il tuo televisore o set-top box in un dispositivo completamente "
"libero. Un grande launcher che fornisce un facile accesso a tutte le "
"applicazioni e alle funzionalità installate. Controllabile tramite voce o "
"telecomando del televisore."

#: i18n/en.yaml:0
msgid "Contribute"
msgstr "Contribuisci"

#: i18n/en.yaml:0
msgid ""
"Using the multi-platform toolkit Qt, the flexible extensions of KDE "
"Frameworks plus the power of Plasma Shell, Plasma Bigscreen is built with "
"technology which feels equally at home on the desktop, mobile devices and TV."
msgstr ""
"Utilizzando il toolkit multi-piattaforma Qt, le estensioni flessibili di KDE "
"Frameworks, oltre alla potenza di Plasma Shell, Plasma Bigscreen è stato "
"creato con una tecnologia che si sente a suo agio su desktop, dispositivi "
"mobili e televisori."

#: i18n/en.yaml:0
msgid "KWin and Wayland"
msgstr "KWin e Wayland"

#: i18n/en.yaml:0
msgid ""
"Wayland is the next-generation protocol for delivering cutting-edge "
"interfaces which are silky smooth. KWin is the battle-tested window manager "
"which implements Wayland, delivering a polished and reliable experience on "
"both the desktop and mobile devices."
msgstr ""
"Wayland è il protocollo di nuova generazione per fornire interfacce "
"all'avanguardia estremamente fluide. KWin è il gestore di finestre testato "
"in battaglia che implementa Wayland e offre un'esperienza raffinata e "
"affidabile sia sui desktop che sui dispositivi mobili."

#: i18n/en.yaml:0
msgid "Making full use of Open Source"
msgstr "Fa uso completo di open source"

#: i18n/en.yaml:0
msgid ""
"Plasma Bigscreen combines many powerful software tools from established "
"projects to make a whole greater than the sum of its parts.<br /> MyCroft "
"allows us to control our TV with just our voice, CEC makes it possible to "
"control the interface using your normal TV remote control, and Pulseaudio "
"drives the Plasma Bigscreen sound system."
msgstr ""
"Plasma Bigscreen combina molti potenti strumenti software di progetti "
"consolidati per creare un insieme maggiore della somma delle sue parti.<br /"
"> MyCroft consente di controllare il nostro televisore con la sola voce, CEC "
"rende possibile il controllo dell'interfaccia utilizzando il normale "
"telecomando del televisore e Pulseaudio gestisce il sistema sonoro di Plasma "
"Bigscreen."

#: i18n/en.yaml:0
msgid "Adapting to your needs"
msgstr "Si adatta alle tue necessità"

#: i18n/en.yaml:0
msgid ""
"Being based on the most flexible desktop in the world means you can truly "
"make your TV your own. Add and modify widgets, change colour schemes, fonts, "
"and much, much more."
msgstr ""
"Essendo basato sul desktop più flessibile del mondo ti consente davvero di "
"personalizzare il tuo televisore. Aggiungi e modifica gli oggetti, modifica "
"gli schemi di colori, i caratteri e molto altro ancora."

#: i18n/en.yaml:0
msgid "What is Plasma Bigscreen?"
msgstr "Che cos'è Plasma Bigscreen?"

#: i18n/en.yaml:0
msgid ""
"Plasma Bigscreen offers a free (as in freedom and beer), user-friendly, "
"privacy-enabling and customizable platform for TV's and setup-boxes. It is "
"shipped by different distributions (ex. postmarketOS, KDE Neon), and can run "
"on the devices that are supported by the distribution."
msgstr ""
"Plasma Bigscreen offre una piattaforma per televisori e set-top box libera e "
"gratuita, amichevole, abilitata alla riservatezza e personalizzabile. Viene "
"fornita da diverse distribuzioni (ad es. postmarketOS, KDE Neon) e può "
"essere eseguita su altri dispositivi che sono supportati dalle distribuzioni."

#: i18n/en.yaml:0
msgid "Why?"
msgstr "Perché?"

#: i18n/en.yaml:0
msgid ""
"The most common offerings on TV's and setup-boxes lack openness and trust. "
"In a world of walled gardens, we want to create a platform that respects and "
"protects the user’s privacy to the fullest. We want to provide a fully open "
"base which others can help develop and use for themselves, or in their "
"products."
msgstr ""
"Le offerte più comuni di televisori e set-top box mancano di apertura e "
"fiducia. In un mondo di giardini recintati, vogliamo creare una piattaforma "
"che rispetti e che protegga al massimo la riservatezza dell'utente. Vogliamo "
"anche fornire una base completamente aperta, in modo che altre persone "
"possano aiutare a svilupparla o a utilizzarla per se stessi o nei loro "
"prodotti."

#~ msgid "Apps"
#~ msgstr "Applicazioni"

#~ msgid ""
#~ "Voice applications do not start on boot and I see a blank screen. What "
#~ "should I do?"
#~ msgstr ""
#~ "Le applicazioni vocali non vengono eseguite all'avvio e vedo una "
#~ "schermata vuota. Cosa dovrei fare?"

#~ msgid ""
#~ "Voice applications are only accessible once MyCroft is ready and has "
#~ "started, you will be notified on the top panel when MyCroft has started "
#~ "and is in a ready state."
#~ msgstr ""
#~ "Le applicazioni vocali sono accessibili solo una volta che MyCroft è "
#~ "pronto e avviato, riceverai una notifica sul pannello superiore quando "
#~ "MyCroft è stato avviato ed è pronto."

#~ msgid ""
#~ "What is the hot key assigned to activate MyCroft voice control without a "
#~ "USB mic remote?"
#~ msgstr ""
#~ "Qual è il tasto di scelta rapida assegnato per attivare il controllo "
#~ "vocale di MyCroft senza un telecomando per microfono USB?"

#~ msgid ""
#~ "There is no hotkey assigned to MyCroft, the Generic USB remotes that have "
#~ "a mic button only activate the mic input on the USB remote hardware, to "
#~ "use MyCroft one needs to activate it with the hot word “Hey Mycroft”."
#~ msgstr ""
#~ "Non esiste un tasto di scelta rapida assegnato a MyCroft, i telecomandi "
#~ "USB generici che hanno un pulsante del microfono attivano solo l'ingresso "
#~ "del microfono sull'hardware del telecomando USB, per utilizzare MyCroft è "
#~ "necessario attivarlo con la frase «Hey Mycroft»."

#~ msgid ""
#~ "To use MyCroft one does not require a USB mic enabled remote "
#~ "specifically, any microphone that can be connected to your device should "
#~ "work."
#~ msgstr ""
#~ "Per utilizzare MyCroft one non è necessario un telecomando abilitato per "
#~ "microfono USB in particolare, qualsiasi microfono che può essere "
#~ "collegato al dispositivo dovrebbe funzionare."

#~ msgid ""
#~ "For issues and troubleshooting your microphone not working one can refer "
#~ "to: https://mycroft-ai.gitbook.io/docs/using-mycroft-ai/troubleshooting/"
#~ "audio-troubleshooting."
#~ msgstr ""
#~ "Per problemi e risoluzione dei problemi del microfono che non funziona, "
#~ "puoi fare riferimento a: https://mycroft-ai.gitbook.io/docs/using-mycroft-"
#~ "ai/troubleshooting/audio-troubleshooting."

#~ msgid "postmarketOS"
#~ msgstr "postmarketOS"

#~ msgid "![](/img/pmOS.svg)"
#~ msgstr "![](/img/pmOS.svg)"

#~ msgid ""
#~ "PostmarketOS (pmOS) is a pre-configured Alpine Linux that can be "
#~ "installed on smartphones, SBC's and other mobile devices. View the "
#~ "[device list](https://wiki.postmarketos.org/wiki/Devices) to see the "
#~ "progress for supporting your device."
#~ msgstr ""
#~ "PostmarketOS (pmOS) è un Alpine Linux preconfigurato che può essere "
#~ "installato su smartphone, SBC e su altri dispositivi mobili. Visualizza "
#~ "la [lista dei dispositivi](https://wiki.postmarketos.org/wiki/Devices) "
#~ "per vedere l'avanzamento del supporto per il tuo dispositivo."

#~ msgid ""
#~ "For devices that do not have prebuilt images, you will need to flash it "
#~ "manually using the `pmbootstrap` utility. Follow instructions [here]"
#~ "(https://wiki.postmarketos.org/wiki/Installation_guide). Be sure to also "
#~ "check the device's wiki page for more information on what is working."
#~ msgstr ""
#~ "Per i dispositivi che non hanno immagini precostituite, sarà necessario "
#~ "scriverla usando il programma «pmbootstrap». Segui [queste](https://wiki."
#~ "postmarketos.org/wiki/Installation_guide) istruzioni. Controlla anche la "
#~ "pagina wiki del dispositivo per ulteriori informazioni su cosa funziona."

#~ msgid "[Learn more](https://postmarketos.org)"
#~ msgstr "[Scopri di più](https://postmarketos.org)"

#~ msgid "Download:"
#~ msgstr "Scarica:"

#~ msgid "[Community Devices](https://postmarketos.org/download/)"
#~ msgstr "[Dispositivi della comunità](https://postmarketos.org/download/)"

#~ msgid "Manjaro ARM"
#~ msgstr "Manjaro ARM"

#~ msgid "![](/img/manjaro.png)"
#~ msgstr "![](/img/manjaro.png)"

#~ msgid ""
#~ "Manjaro ARM is an Arch based distribution with support for a wide range "
#~ "of single board computers, ARM laptops and phones."
#~ msgstr ""
#~ "Manjaro ARM è una distribuzione basata su Arch con supporto per un'ampia "
#~ "gamma di computer a scheda singola, laptop e telefoni ARM."

#~ msgid ""
#~ "Manjaro ARM currently offers Bigscreen images for Odroid N2 and N2+, "
#~ "Radxa Zero 2, RockPro64, Raspberry Pi 4, Khadas Vim 1 and Khadas Vim 3."
#~ msgstr ""
#~ "Manjaro ARM offre attualmente immagini di Bigscreen per Odroid N2 e N2+, "
#~ "Radxa Zero 2, RockPro64, Raspberry Pi 4, Khadas Vim 1 e Khadas Vim 3."

#~ msgid ""
#~ "[Development images](https://github.com/manjaro-arm/bigscreen/releases)"
#~ msgstr ""
#~ "[Immagini di sviluppo](https://github.com/manjaro-arm/bigscreen/releases)"

#~ msgid "Installation"
#~ msgstr "Installazione"

#~ msgid ""
#~ "Simply download the Bigscreen image for the device you have, extract it "
#~ "so you have the raw .img file, then flash it to your drive using `dd` or "
#~ "a graphical tool. Then plug in the drive to your device and power it on."
#~ msgstr ""
#~ "Scarica semplicemente l'immagine di Bigscreen per il dispositivo che hai, "
#~ "estraila in modo da avere il file .img, quindi scrivi l'immagine sul tuo "
#~ "disco usando «dd» o uno strumento grafico. Quindi collega l'unità al "
#~ "dispositivo e accendila."

#~ msgid "Debian"
#~ msgstr "Debian"

#~ msgid "![](/img/debian.png)"
#~ msgstr "![](/img/debian.png)"

#~ msgid ""
#~ "Debian has Plasma Bigscreen available as a [package ](https://packages."
#~ "debian.org/stable/kde/plasma-bigscreen) in their repositories."
#~ msgstr ""
#~ "Debian ha Plasma Bigscreen disponibile come [pacchetto](https://packages."
#~ "debian.org/stable/kde/plasma-bigscreen) nei loro depositi."

#~ msgid "Currently, Plasma Bigscreen runs on the following device types:"
#~ msgstr ""
#~ "Attualmente, Plasma Bigscreen funziona sui seguenti tipi di dispositivi:"

#~ msgid ""
#~ "**(Recommended) Raspberry Pi 4:** We offer official images built for the "
#~ "Raspberry Pi 4 on top of KDE Neon."
#~ msgstr ""
#~ "**(Consigliato) Raspberry Pi 4:** Offriamo immagini ufficiali create per "
#~ "Raspberry Pi 4 su KDE Neon."

#~ msgid ""
#~ "**postmarketOS devices:** postmarketOS is a distribution based on Alpine "
#~ "Linux that can be installed on Android smartphones but also on SBC's like "
#~ "the Raspberry Pi 4 and other devices. Please find your device from the "
#~ "[list of supported devices](https://wiki.postmarketos.org/wiki/Devices) "
#~ "and see what's working, then you can follow the [pmOS installation guide]"
#~ "(https://wiki.postmarketos.org/wiki/Installation_guide) to install it on "
#~ "your device. Your mileage may vary depending on the device in use and "
#~ "when you use a device in the testing category it is **not** necessarily "
#~ "representative of the current state of Plasma Bigscreen."
#~ msgstr ""
#~ "**Dispositivi con postmarketOS:** postmarketOS è una distribuzione basata "
#~ "su Alpine Linux che può essere installata su smartphone Android, ma anche "
#~ "su SBC come il Raspberry Pi 4  e su altri dispositivi. Cerca il tuo "
#~ "dispositivo nell'[elenco dei dispositivi supportati](https://wiki."
#~ "postmarketos.org/wiki/Devices) e vedi che cosa funziona, poi potrai "
#~ "seguire la [guida all'installazione di pmOS](https://wiki.postmarketos."
#~ "org/wiki/Installation_guide) per installarlo sul tuo dispositivo. La tua "
#~ "esperienza potrebbe essere diversa a seconda del dispositivo in uso, e, "
#~ "se stai usando un dispositivo nella categoria di prova, **non** è "
#~ "necessariamente rappresentativa dello stato attuale di Plasma Bigscreen."

#~ msgid "I've installed Plasma Bigscreen, what is the login password?"
#~ msgstr "Ho installato Plasma Bigscreen, qual è la password di accesso?"

#~ msgid ""
#~ "If you've installed the reference KDE Neon image for the Raspberry Pi 4, "
#~ "the password and username are both \"mycroft\", and you can then change "
#~ "the password afterwards by running \"passwd\" from a tty."
#~ msgstr ""
#~ "Se hai installato l'immagine di riferimento KDE Neon per Raspberry Pi 4, "
#~ "la password e il nome utente sono entrambi «mycroft» e puoi quindi "
#~ "modificare la password in seguito eseguendo «passwd» da un terminale."

#~ msgid "Neon based reference rootfs"
#~ msgstr "rootfs di riferimento basato su Neon"

#~ msgid "![](/img/neon.svg)"
#~ msgstr "![](/img/neon.svg)"

#~ msgid ""
#~ "Image based on KDE Neon. KDE Neon itself is based upon Ubuntu 20.04 "
#~ "(focal). This image is based on the dev-unstable branch of KDE Neon, and "
#~ "always ships the latest versions of KDE frameworks, KWin and Plasma "
#~ "Bigscreen compiled from git master."
#~ msgstr ""
#~ "Immagine basata su KDE Neon. Lo stesso KDE Neon è basato su Ubuntu 20.04 "
#~ "(focal). Questa immagine è basata sul ramo dev-unstable di KDE Neon e "
#~ "fornisce sempre l'ultima versione di KDE frameworks, di KWin e di Plasma "
#~ "Bigscreen compilate da git master."

#~ msgid ""
#~ "[Raspberry Pi 4](https://sourceforge.net/projects/bigscreen/files/rpi4/"
#~ "beta)"
#~ msgstr ""
#~ "[Raspberry Pi 4](https://sourceforge.net/projects/bigscreen/files/rpi4/"
#~ "beta)"

#~ msgid ""
#~ "Download the image, uncompress it and flash it to a SD-card using `dd` or "
#~ "a graphical tool. The Raspberry Pi will automatically boot from a SD-Card."
#~ msgstr ""
#~ "Scarica l'immagine, decomprimila e scrivila su una scheda SD usando «dd» "
#~ "oppure uno strumento grafico,: Raspberry Pi si avvierà automaticamente da "
#~ "lì."

#~ msgid "For a complete step-by-step readme, [click here](/manual)."
#~ msgstr "Per una guida completo passo dopo passo, [fai clic qui](/manual)."
